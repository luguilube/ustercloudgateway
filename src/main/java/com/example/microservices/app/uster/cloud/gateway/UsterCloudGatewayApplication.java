package com.example.microservices.app.uster.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class UsterCloudGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsterCloudGatewayApplication.class, args);
	}

}
